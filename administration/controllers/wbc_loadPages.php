<?php

class WBC_load_pages
{
    function __construct()
    {
    }
    final function wbc_load_page($page)
    {
        $wbc_database = new WBC_BaseDatosController();
        wp_enqueue_style(
            'wbc_boostrapAdministracion',
            plugin_dir_url(__FILE__) . '../static/pluggins/bootstrap/css/bootstrap.min.css',
            array(),
            "1.0.0"
        );
        wp_enqueue_script(
            'wbc_script_boostrapjs',
            plugins_url('../static/pluggins/bootstrap/js/bootstrap.min.js', __FILE__),
            array('jquery')
        );

        if ($page == "proyectos.phtml") {


            wp_enqueue_style(
                'wbc_style',
                plugin_dir_url(__FILE__) . '../static/css/style.css',
                array(),
                "1.0.0"
            );

            $listar_proyectos = $wbc_database->wbc_listar_bd('wbc_proyectos');

          $auxiliar = isset($_POST['id_proyecto']) ? "update" : "add";
            if (isset($_POST['id_proyecto'])) {
                $listar_proyectosd = $wbc_database->wbc_listar_columnas_bd('*', 'wbc_proyectos', 'WHERE proyecto_id=' . $_POST['id_proyecto']);
                foreach ($listar_proyectosd as $value) {
                    $id_proyecto = $value->proyecto_id;
                    $title_proyecto = $value->title_proyecto;
                    $link_video = $value->link_video;
                    $costo_proyecto = $value->costo_proyecto;
                    $descrip_proyecto = $value->descrip_proyecto;
                    $url_video = $value->url_video;
                    $forma_pago = $value->forma_pago;
                    $link_descarga = $value->link_descarga;
                    $link_pago = $value->link_pago;
                    $fecha_registro = $value->fecha_registro;
                }
            }
        }
        require_once plugin_dir_path(__FILE__) . '../views/' . $page;
    }
    final function wbc_presenta_mensaje()
    {
        if (isset($_SESSION["wbc_mensaje"])) {
            echo ('<div class="alert alert-' . ($_SESSION['wbc_mensaje']['status']) . '">    
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>' . __('Aviso:') . '</strong> ' . esc_attr($_SESSION["wbc_mensaje"]['response']) . '
            </div>');
            $_SESSION["wbc_mensaje"] = null;
        }
    }
}
