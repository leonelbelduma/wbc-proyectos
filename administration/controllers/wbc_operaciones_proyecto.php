<?php
class WBC_operacionesProyecto
{
    function wbc_admin_redireccionamiento($argumento = "")
    {
        ob_start();
        $url_redirect = (empty(admin_url()) ? network_admin_url($argumento) : admin_url($argumento));
        wp_redirect($url_redirect);
        exit;
    }

    final function wbc_envia_mensaje($mensaje, $status)
    {
        $_SESSION["wbc_mensaje"] = array('response' => $mensaje, 'status' => $status);
    }

    function wbc_save_proyecto()
    {
        $wbc_database = new WBC_BaseDatosController();
        if (isset($_POST['tituloproyecto']) && !empty($_POST['desc_proyecto'])) {
            $linkvideo =   str_replace("\"", "'", $_POST['linkvideo']);
            $linkvideo =   str_replace("560", "100%",   $linkvideo);
            $wbc_data = [
                'title_proyecto' => sanitize_text_field(($_POST['tituloproyecto'])),
                'link_video' => stripslashes($linkvideo),
                'costo_proyecto' => $_POST['costo_proyecto'],
                'descrip_proyecto' => stripslashes($_POST['desc_proyecto']),
                'url_video' => $_POST['url_video'],
                'forma_pago' => $_POST['form_pago'],
                'link_descarga' => $_POST['link_descarga'],
                'link_pago' => $_POST['link_pago']
            ];
            $bandera = $wbc_database->wbc_agregar_bd("wbc_proyectos", $wbc_data);
            if ($bandera) {
                $this->wbc_envia_mensaje(__("Datos registrados"), "success");
            } else {
                $this->wbc_envia_mensaje(__("Error al registrar datos"), "danger");
            }
        } else {
            $this->wbc_envia_mensaje(__("Ingresar todos los datos"), "danger");
        }
        $this->wbc_admin_redireccionamiento("/admin.php?page=wbc_menu");
    }

    function wbc_actualizar_proyecto()
    {
        $wbc_database = new WBC_BaseDatosController();
        if (isset($_POST['tituloproyecto']) && !empty($_POST['desc_proyecto'])) {
            $id_proyecto = sanitize_text_field($_POST['id_cliente']);
            $title_proyecto = sanitize_text_field(($_POST['tituloproyecto']));
            $link_video =   str_replace("\"", "'", $_POST['linkvideo']);
            $link_video =   str_replace("560", "100%",  $link_video);
            $costo_proyecto = $_POST['costo_proyecto'];
            $descrip_proyecto = stripslashes($_POST['desc_proyecto']);
            $url_video = $_POST['url_video'];
            $forma_pago = $_POST['form_pago'];
            $link_descarga = $_POST['link_descarga'];
            $link_pago = $_POST['link_pago'];
            if ($wbc_database->wbc_modificar_bd(
                'wbc_proyectos',
                array(
                    'title_proyecto' =>  $title_proyecto,
                    'link_video' =>  stripslashes($link_video),
                    'costo_proyecto' => $costo_proyecto,
                    'descrip_proyecto' => $descrip_proyecto,
                    'url_video' =>  $url_video,
                    'forma_pago' => $forma_pago,
                    'link_descarga' => $link_descarga,
                    'link_pago' =>  $link_pago
                ),
                array("proyecto_id" =>  $id_proyecto)
            )) {

                $this->wbc_envia_mensaje(__("Datos Actualizados"), "success");
            } else {
                $this->wbc_envia_mensaje(__("Error al actualizar datos"), "danger");
            }
        } else {
            $this->wbc_envia_mensaje(__("Ingresar todos los datos"), "danger");
        }
        $this->wbc_admin_redireccionamiento("/admin.php?page=wbc_menu");
    }


    function wbc_admin_proyecto()
    {
        if (isset($_POST["crud"]) && $_POST["crud"] == "add") {
            $this->wbc_save_proyecto();
        } else if (isset($_POST["crud"]) && $_POST["crud"] == "update") {
            $this->wbc_actualizar_proyecto();
        } else {
            $this->wbc_envia_mensaje(__("Error al realizar la operación "), "danger");
            $this->wbc_admin_redireccionamiento("/admin.php?page=wbc_menu");
        }
    }
}
