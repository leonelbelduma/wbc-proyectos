<?php
add_action('admin_menu', 'wbc_menu_admin');
function wbc_menu_admin()
{
    add_menu_page(
        'WB-easy proyect',
        'WB-easy proyect',
        'manage_options',
        'wbc_menu',
        'wbc_submenu_principal',
        plugin_dir_url(__FILE__) . '/static/images/logo_wrwt.png',
        '5'
    );
}

function wbc_submenu_principal()
{
    $wbc_load_page = new  WBC_load_pages();
    $wbc_load_page->wbc_load_page("proyectos.phtml");
}


add_action('admin_post_wbc_admin_proyecto', 'wbc_admin_proyecto');
function wbc_admin_proyecto()
{
    $producto = new WBC_operacionesProyecto();
    $producto->wbc_admin_proyecto();
}
