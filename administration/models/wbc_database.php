<?php

class WBC_BaseDatosController
{
    private $wpdb_local;
    function __construct()
    {
        global $wpdb;
        $this->wpdb_local = $wpdb;
    }

    function wbc_crear_tablas()
    {
        $charset_collate = $this->wpdb_local->get_charset_collate();
        $table_proyectos = $this->wpdb_local->prefix . 'wbc_proyectos';
        $sql_portafolio = "CREATE TABLE " . $table_proyectos . " (
            proyecto_id int(11) NOT NULL AUTO_INCREMENT,
            title_proyecto TEXT NOT NULL,
            link_video TEXT NOT NULL,
            costo_proyecto double NOT NULL,
            descrip_proyecto TEXT NOT NULL,
            forma_pago varchar(250),
            url_video TEXT NOT NULL,
            link_descarga TEXT NOT NULL,
            link_pago TEXT NOT NULL,
            fecha_registro DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (proyecto_id)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql_portafolio);
    }

    function wbc_agregar_bd($tabla, $datos)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->insert($tabla, $datos);
    }
    function wbc_modificar_bd($tabla, $datos, $where)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->update($tabla, $datos, $where);;
    }
    function wbc_eliminar_bd($tabla, $where)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->delete($tabla, $where);
    }
    function wbc_get_prefijo_table()
    {
        return $this->wpdb_local->prefix;
    }
    function wbc_listar_columnas_bd($columnas, $tabla, $where = "")
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->get_results('SELECT ' . $columnas . ' FROM ' . $tabla . ' ' . $where . ' ;');
    }

    function wbc_listar_bd($tabla)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->get_results('SELECT * FROM ' . $tabla, ARRAY_A);
    }
}
