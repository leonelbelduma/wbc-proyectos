<?php

/**
 * Plugin Name: WBC Proyectos
 * Plugin URI: https://wibcode.com/
 * Description: Plugin permite ingresar diferentes proyectos con descripción. 
 * Version: 1.0.0
 * Author: Wibcode
 * Author https://wibcode.com/
 * License: GPL2
 * Text Domain: wbc-proyectos
 * Domain Path: /languages
 */
defined('ABSPATH') or die('');
register_activation_hook(__FILE__, 'wbc_activar');

function wbc_activar()
{
    $basedatos = new WBC_BaseDatosController();
    $basedatos->wbc_crear_tablas();
}

require_once plugin_dir_path(__FILE__) . 'administration/models/wbc_database.php';
if (is_admin()) {
  
    require_once plugin_dir_path(__FILE__) . 'administration/controllers/wbc_loadPages.php';
    require_once plugin_dir_path(__FILE__) . 'administration/wbc_hook.php';
    require_once plugin_dir_path(__FILE__) . 'administration/controllers/wbc_operaciones_proyecto.php';
}
require_once plugin_dir_path(__FILE__) . 'public/wbc_hook.php';
require_once plugin_dir_path(__FILE__) . 'public/controllers/wbc_loadPages.php';

function wbc_session()
{
    if (!session_id() && is_user_logged_in()) {
        session_start();
    }
}
add_action('init', 'wbc_session', 1);
