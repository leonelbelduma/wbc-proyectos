<?php
class WBC_shortcodeLoadPageController
{
    function __construct()
    {
    }

    final function load_page($page)
    {
        if ($page == "proyectos.phtml") {

            $wbc_database = new WBC_BaseDatosController();
            $listar_proyectos = $wbc_database->wbc_listar_bd('wbc_proyectos');  

            wp_enqueue_style(
                'wbc-style-public',
                plugin_dir_url(__FILE__) . '../static/css/style-shortcode.css',
                array(),
                "1.0.0"
            );
            wp_enqueue_script(
                'wbc_script_fontawasome',
                plugins_url('../../administration/static/pluggins/fontawasome/all.js', __FILE__),
                array('jquery')
            );
        }
        require_once plugin_dir_path(__FILE__) . '../views/' . $page;
    }

   
}
